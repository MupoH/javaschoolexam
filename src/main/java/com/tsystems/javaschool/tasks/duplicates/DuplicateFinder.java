package com.tsystems.javaschool.tasks.duplicates;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import static java.util.Collections.sort;

public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) {
        List<String> list = new ArrayList<>();
        Map<String,Integer> map=new TreeMap<>();

        if (sourceFile == null)
            throw new IllegalArgumentException();

        if (targetFile == null) {
            throw new IllegalArgumentException();
        }

        if (!targetFile.exists()){
            File tfile = new File(targetFile.getPath());
            targetFile = tfile;
        }

        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(sourceFile), StandardCharsets.UTF_8))) {
            String line;
            while ((line = reader.readLine()) != null) {
                list.add(line);
            }
            sort(list);
        } catch (IOException e) {
            e.printStackTrace();
        }

        for(String s:list){
            map.put(s,Collections.frequency(list, s));
        }

        try (BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(
                        new FileOutputStream(targetFile), StandardCharsets.UTF_8))) {
                for(Map.Entry<String,Integer> entry:map.entrySet()) {
                    writer.write(entry.getKey() + "[" + entry.getValue() + "]");
                    writer.newLine();
                }
                writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return true;
    }
}
