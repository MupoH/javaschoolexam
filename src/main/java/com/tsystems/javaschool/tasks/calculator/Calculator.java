package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        try {
            if (!isStatementCorrect(statement)) return null;
            String rpn = parseToRPN(statement);
            double result = counting(rpn);
            return formatDoubleResult(result);
        }
        catch (NumberFormatException e) {
            return null;
        }
    }

    private String parseToRPN(String s){
        final StringBuilder rpn = new StringBuilder();
        final Stack<Character> op = new Stack<>();
        for (int i = 0; i<s.length(); i++){
            if (isOperator(s.charAt(i))){
                if (s.charAt(i) == '(') op.push(s.charAt(i));
                else if (s.charAt(i) == ')'){
                    char c = op.pop();
                    while (c != '('){
                        rpn.append(c + " ");
                        c = op.pop();
                    }
                }
                else {
                    if (!op.isEmpty())
                        if (priority(s.charAt(i)) <= priority(op.peek())){
                        rpn.append(op.pop() + " ");
                    }
                    op.push(s.charAt(i));
                }

            }
            else {
                while (!isOperator(s.charAt(i))){
                    rpn.append(s.charAt(i));
                    i++;
                    if (i == s.length()) break;
                }
                rpn.append(" ");
                i--;
            }
        }
        while (!op.isEmpty()) rpn.append(op.pop() + " ");
        return rpn.toString();
    }

    private double counting(String rpn){
        double result = 0;
        Stack<Double> temp = new Stack<>();
        String[] expr = rpn.split(" ");
        for (String i : expr){
            if (isNumber(i)) temp.push(Double.parseDouble(i));
            else {
                double a = temp.pop();
                double b = temp.pop();
                switch (i) {
                    case "+":
                        result = b + a;
                        break;
                    case "-":
                        result = b - a;
                        break;
                    case "*":
                        result = b * a;
                        break;
                    case "/":
                        result = b / a;
                        break;
                }
                temp.push(result);
            }
        }
        return temp.peek();
    }

    private boolean isStatementCorrect(String statement) {
        if (statement == null || statement.isEmpty()) {
            return false;
        }

        for (int i=0; i<statement.length(); i++)
            if (statement.charAt(i) != ')')
                if (isOperator(statement.charAt(i)) && isOperator(statement.charAt(++i)))
                    if (statement.charAt(i) != '(')return false;

        int countLBracket=0, countRBracket = 0;
        for (char element : statement.toCharArray()){
            if (element == ',') return false;
            if (element == ')') countRBracket++;
            if (element == '(') countLBracket++;
        }
        return countLBracket == countRBracket;
    }

    private static boolean isNumber(String str)
    {
        for (char c : str.toCharArray())
        {
            if (!Character.isDigit(c)) if (!(c=='.'))return false;
        }
        return true;
    }

    private String formatDoubleResult(double d) {
        if (Double.isInfinite(d)) {
            return null;
        }
        if (d == (long) d) {
            return String.valueOf((long) d);
        } else {
            return String.valueOf(d);
        }
    }

    private static boolean isOperator(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/' || c == '(' || c == ')';
    }

    private static int priority(char op) {
        switch (op) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
                return 2;
            default:
                return -1;
        }
    }
}
