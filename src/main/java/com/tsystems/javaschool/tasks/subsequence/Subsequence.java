package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        if (x == null || y == null) throw new IllegalArgumentException();

        Iterator iterY = y.iterator();
        Iterator iterX = x.iterator();
        Object curX;
        Object curY;

        while (iterX.hasNext()) {
            curX = iterX.next();
            while (iterY.hasNext()) {
                curY = iterY.next();
                if (curX.equals(curY)) {
                    iterX.remove();
                    break;
                }
            }
        }
        return x.isEmpty();
    }
}
